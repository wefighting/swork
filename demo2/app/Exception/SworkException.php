<?php
namespace Demo2\App\Exception;

use Swork\Server\Http\Argument;

class SworkException
{
    public function handler(Argument $argument, \Throwable $ex)
    {
        return [
            'status' => $ex->getCode(),
            'msg' => $ex->getMessage()
        ];
    }
}
