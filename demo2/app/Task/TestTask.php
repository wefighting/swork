<?php
namespace Demo2\App\Task;

use Demo\App\Model\TestModel;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\Annotation\TimerTask;
use Swork\Bean\Annotation\Reference;
use Swork\Bean\BeanCollector;
use Swork\Db\Db;

class TestTask extends BeanCollector
{
    /**
     * @Inject()
     * @var TestModel
     */
    public $testModel;

    /**
     * @Reference("user")
     * @var \Demo\App\Service\TestInterface
     */
    public $testInterface;

    /**
     * 每2秒
     * @TimerTask(2000)
     */
    function test01()
    {
        echo '@TimerTask-1'. PHP_EOL;
//        $result = $this->testInterface->info(1, 2);

        sleep(3);
    }

    /**
     * 本地普通任务，在本服务内完成
     * @TimerTask(1000)
     */
    function test02()
    {
        echo '@TimerTask-2'. PHP_EOL;
        Db::beginTransaction();
        try {
//            $count = $this->testModel->getCount();
            $this->testModel->insert([
                'name' => 'qingqingqing'
            ]);
            Db::commit();
        } catch (\Throwable $e) {
            Db::rollback();
        }
//        var_dump($count);
    }

    /**
     * 分布式普通任务，自动分发至可用服务器之下
     * @TimerTask(2000)
     */
    function test03()
    {
        echo '@TimerTask-3'. PHP_EOL;
    }

    /**
     * 分布式裂变式的任务，在执行完后，会生产多个子任务（每2秒执行一次）
     * @TimerTask("group1", 2000)
     */
    function test04()
    {
        echo '@TimerTask-4'. PHP_EOL;
        return [1,3,4,5];
    }

    /**
     * 分布式裂变式的任务，在执行完后，会生产多个子任务（不自动执行）
     * @TimerTask("group1", 0)
     */
    function test07()
    {
        echo '@TimerTask-7'. PHP_EOL;
        return [1,3,4,5];
    }

    /**
     * 分布式子任务（与裂变式的任务配合）
     * @TimerTask("group1")
     * @param $arg
     */
    function test05($arg)
    {
        echo '@TimerTask-5'. PHP_EOL;
    }

    /**
     * @TimerTask(2000)
     */
    function test06()
    {
        echo '@TimerTask-6'. PHP_EOL;
        return false;
    }
}
