<?php
namespace Demo2\App\Controller;

use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/")
 */
class IndexController extends BeanCollector
{

    public function index(Argument $arg)
    {
        return ['demo2', uniqid(), microtime()];
    }
}
