<?php
return [
    'uri' => [
        'host' => '127.0.0.1',
        'port' => 6379,
    ],
    'pools' => 50,
    'prefix' => ''
];
/*
return [
    'uri' => [
        [
            'host' => '10.20.1.232',
            'port' => 6379
        ],
        [
            'host' => '10.20.1.233',
            'port' => 6379
        ],
        [
            'host' => '10.20.1.247',
            'port' => 6379
        ],
    ],
    'pools' => 50,
    'prefix' => ''
];
*/