<?php
namespace Demo\App\Amqp;

use Swork\Bean\Annotation\Inject;
use Swork\Bean\Annotation\QueueTask;
use Swork\Bean\Annotation\TimerTask;
use Swork\Bean\BeanCollector;
use Swork\Client\Amqp;
use Swork\Queue\AmqpArgument;

class TestAmqp extends BeanCollector
{
    /**
     * @Inject("send_sms")
     * @var Amqp
     */
    private $amqp;

    /**
     * @Inject("send_sms2")
     * @var Amqp
     */
    private $amqp2;

    /**
     * @param AmqpArgument $argument
     * @QueueTask("send_sms")
     * @QueueTask("consumer_tag", "")
     * @QueueTask("no_local", false)
     * @QueueTask("no_ack", false)
     * @QueueTask("exclusive", false)
     * @QueueTask("nowait", false)
     * @return bool
     * @throws
     */
    function test01(AmqpArgument $argument)
    {
        $channel = $argument->getChannel();
        $message = $argument->getMessage();
        $body = $argument->getBody();

        //发送成功
        var_dump('[x1] 发送成功！');
        var_dump($body);

        //返回
        return true;
    }

    /**
     * @TimerTask(2000)
     * @throws
     */
    function send()
    {
        $this->amqp->basePublish('xxxxxxxxxxxxx');
        sleep(1);
        $this->amqp2->basePublish('yyyyyyyyyy');
    }
}
