<?php
namespace Demo\App\Model;

use Swork\Db\MySqlModel;

class SortModel extends MySqlModel
{
    public function __construct()
    {
        $tbl = 'sort';
        $key = ['sid', MySqlModel::AutoKeyID];
        $cols = [
            'sid' => ['i', 0],
            'sname' => ['s', ''],
            'atime' => ['i', 0]
        ];
        $inc = '';
        parent::__construct($tbl, $key, $cols, $inc);
    }
}
