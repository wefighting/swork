<?php
namespace Demo\App\Process;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Swork\Bean\BeanCollector;
use Swork\Client\Amqp;
use Swork\Client\Redis;
use Swork\Process\ProcessHandlerInterface;
use Swork\Bean\Annotation\Process;
use Swork\Bean\Annotation\Inject;
use Swork\Service;

/**
 * Class AmqpProcessHandler
 * @UserP111rocess()
 */
class AmqpProcessHandler extends BeanCollector implements ProcessHandlerInterface
{
    /**
     * @Inject()
     * @var Redis
     */
    private $redis;

    /**
     * @Inject()
     * @var Amqp
     */
    private $amqp;

    /**
     * @param \swoole_websocket_server $server
     * @param \swoole_process $process
     * @param array $exts
     * @throws
     */
    public function run(\swoole_websocket_server $server, \swoole_process $process, array $exts = [])
    {
//        $connection = $this->amqp->getAMQPStreamConnection();
//        $channel = $connection->channel();
//
//        //队列声明
//        $queue_name = 'task_queue' . 1;
//        $channel->queue_declare($queue_name, false, true, false, false);
//
//        //注册消费者
//        $callback = function ($msg) {
//            echo ' [x] ', $msg->body, "\n";
//            Service::$server->push(2, 'xxx');
//
//        };
//        $channel->basic_consume($queue_name, '', false, true, false, false, $callback);
//
//        //阻塞
//        while ($channel->is_consuming())
//        {
//            $channel->wait();
//        }
//
//        var_dump('AMQPStream end >>> ' . microtime(true));
//
//        //关闭连接
//        $channel->close();
    }
}
