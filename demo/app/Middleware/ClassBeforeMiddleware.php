<?php
namespace Demo\App\Middleware;

use Swork\Bean\BeanCollector;
use Swork\Middleware\BeforeMiddlewareInterface;
use Swork\Server\ArgumentInterface;

/**
 * 权限校验中间件
 * @Annotation
 */
class ClassBeforeMiddleware extends BeanCollector implements BeforeMiddlewareInterface
{
    /**
     * 中间件处理层
     * @param ArgumentInterface $argument 请求参数
     */
    public function process(ArgumentInterface $argument)
    {
        echo 'ClassBeforeMiddleware - '. json_encode($argument->param()). PHP_EOL;
    }
}
