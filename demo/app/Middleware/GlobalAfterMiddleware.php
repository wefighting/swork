<?php
namespace Demo\App\Middleware;

use Swork\Bean\BeanCollector;
use Swork\Middleware\AfterMiddlewareInterface;
use Swork\Server\ArgumentInterface;

/**
 * 权限校验中间件
 * @Annotation
 */
class GlobalAfterMiddleware extends BeanCollector implements AfterMiddlewareInterface
{
    /**
     * 中间件处理层
     * @param ArgumentInterface $argument 请求参数
     * @param mixed $result 逻辑处理后的结果
     */
    public function process(ArgumentInterface $argument, &$result)
    {
        echo 'GlobalAfterMiddleware - '. json_encode($argument->param()). PHP_EOL;
    }
}
