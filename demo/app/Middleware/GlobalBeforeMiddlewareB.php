<?php
namespace Demo\App\Middleware;

use Demo\App\Breaker\HttpGetBreaker;
use Swork\Bean\Annotation\Breaker;
use Swork\Bean\BeanCollector;
use Swork\Middleware\BeforeMiddlewareInterface;
use Swork\Server\ArgumentInterface;

/**
 * 权限校验中间件
 * @Annotation
 */
class GlobalBeforeMiddlewareB extends BeanCollector implements BeforeMiddlewareInterface
{
    /**
     * 中间件处理层
     * @param ArgumentInterface $argument 请求参数
     * @Breaker(HttpGetBreaker::class)
     */
    public function process(ArgumentInterface $argument)
    {
        echo 'GlobalBeforeMiddlewareB - '. json_encode($argument->param()). PHP_EOL;
    }
}
