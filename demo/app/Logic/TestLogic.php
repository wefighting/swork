<?php
namespace Demo\App\Logic;

use Demo\App\Data\TestData;
use Demo\App\Model\TestModel;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\BeanCollector;
use Swork\Context;
use Swork\Db\Db;
use Swork\Db\MySqlTransaction;
use Swork\Db\Trans;
use Swork\Exception\DbException;

class TestLogic extends BeanCollector
{
    /**
     * @Inject()
     * @var TestModel
     */
    var $testModel;

    /**
     * @Inject()
     * @var TestData
     */
    var $testData;

    /**
     * 插入数据
     */
    function insert()
    {
        $data = [
            'name' => 'test uname',
            'desc' => time(),
            'age' => 21,
            'sex' => 3,
        ];
        return $this->testModel->insert($data);
    }

    /**
     * 获取列表数据
     * @throws
     */
    function getRow()
    {
        return [
            $this->testModel->getRow(['sex' => ['in' => [31, 11]]]),
            $this->testModel->getRow(['sex' => ['in' => [3]]]),
            $this->testModel->getRow(['sex' => ['in' => [3, 5, 6]]]),
            $this->testModel->getRow()
        ];
    }

    /**
     * 获取列表数据
     * @throws
     */
    function getList()
    {
        return [
            $this->testModel->getList(['sex' => ['in' => [31, 11]]]),
            $this->testModel->getList(['sex' => ['in' => [3]]]),
            $this->testModel->getList(['sex' => ['in' => [3, 5, 6]]]),
            $this->testModel->getList([], '*', [], 2)
        ];
    }

    /**
     * 获取列表数据
     * @throws
     */
    function getCount()
    {
        return [
            $this->testModel->getCount(['sex' => ['in' => [3, 2]], 'name' => ['like' => '%123%']]),
            $this->testData->lists(),
            Context::get('context-key')
        ];
    }

    /**
     * 获取列表数据
     * @throws
     */
    function update()
    {
        return $this->testModel->update(['sex' => ['in' => [3, 2]]], ['name' => 'test update 2,3']);
    }

    /**
     * 获取列表数据
     * @throws
     */
    function delete()
    {
        return $this->testModel->delete(['sex' => ['in' => [3]]]);
    }

    /**
     * 事务
     * @throws
     */
    function trans()
    {
        Db::beginTransaction();

        $id = $this->testModel->insert(['name' => 'aaaa']);

        $list = $this->testModel->delete(['sex' => ['in' => [3]]]);
        var_dump($list);

        Db::commit();
    }
}
