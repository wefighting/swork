<?php
namespace Demo\App\Logic;

use Demo\App\Model\TestModel;
use Demo\App\Service\TestInterface;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\Annotation\Service;
use Swork\Bean\BeanCollector;

/**
 * Class TestService
 * @Service()
 * @package Demo\App\Logic
 */
class TestService extends BeanCollector implements TestInterface
{
    /**
     * @Inject()
     * @var TestModel
     */
    public $testModel;

    function info(int $a, int $b)
    {
        return [$a, $b, ($a + $b) * 100];
    }

    /**
     * @return int
     * @throws
     */
    public function mysql()
    {
        // TODO: Implement mysql() method.
        return $this->testModel->getCount();
    }

    public function redis()
    {
        // TODO: Implement redis() method.

    }
}
