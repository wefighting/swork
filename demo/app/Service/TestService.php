<?php
namespace Demo\App\Service;

use Demo\App\Exception\AppException;
use Demo\App\Model\TestModel;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\Annotation\Service;
use Swork\Bean\BeanCollector;
use Swork\Client\Redis;
use Swork\Db\Db;

/**
 * 平台渠道
 * @Service()
 */
class TestService extends BeanCollector implements TestInterface
{
    /**
     * @Inject()
     * @var TestModel
     */
    public $testModel;

    /**
     * @Inject()
     * @var Redis
     */
    public $redis;

    public function info(int $a, int $b)
    {
        Db::beginTransaction();
        try {
            $this->testModel->insert([
                'age' => 18,
                'name' => 'fengwanqing',
                'sex' => 1,
                'desc' => 'Man'
            ]);
            Db::commit();
        } catch(\Throwable $e) {
            Db::rollback();
        }
        return $a + $b;
    }

    public function mysql()
    {
        return $this->testModel->getRow();
    }

    public function redis()
    {
        return $this->redis->set('lalala', 'value');
    }
}
