<?php
namespace Demo\App\Service;

/**
 * 测试接口
 */
interface TestInterface
{
    public function info(int $a, int $b);

    public function mysql();

    public function redis();
}
