{include file="header.tpl" dir="root"}
<h3>foreach</h3>
<p>
    {foreach value=item1 from=$list}
        <div>web.num：{$web.num}</div>
        <div>{$item1.key} - {$item1.value}</div>
        {foreach value=item2 from=$item1.list}
            <div>web.num：{$web.num}</div>
            <div>{$item2.key} - {$item2.value}</div>
            {foreach value=item3 index=idx from=$item2.list}
                <div>idx：{$idx}</div>
                <div>web.num：{$web.num}</div>
                <div>{$item1.key} - {$item2.key} - {$item3.key} - {$item3.value}</div>
                {if $item3.pops == 1000}
                    <div>item3.pops == 1000：{$item3.pops}</div>
                {/if}
                {if $item3.pops >= 1500}
                    <div>item3.pops >= 1500：{$item3.pops}</div>
                {/if}
            {/foreach}
            <p>foreach</p>
        {/foreach}
        <p>foreach</p>
    {/foreach}
</p>
<h3>if</h3>
<p>
    {if $web.num > 9}
        <div>if web.num > 9：{$web.num}</div>
        {if $web.num > 10}
            <div>web.num > 10</div>
        {else}
            <div>web.num <= 10</div>
        {/if}
    {else}
        <div>web.num <= 9：{$web.num}</div>
    {/if}
</p>
<h3>Assign</h3>
<p>{$agn.list[0].key}</p>
<p>{$agn.list[1].value}</p>
<p>{$agn.list[1].value|count}</p>
<p>{$web.time|date:Y-m-d}</p>
<p>{$web.num|add:1}</p>
<p>{$web.title|count}</p>
<p>{$txt[1]|count}</p>
{include file="../pub.tpl"}
{include file="footer.tpl"}