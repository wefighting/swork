<?php
namespace Demo\App\Data;

use Demo\App\Model\TestModel;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\BeanCollector;

class TestData extends BeanCollector
{
    /**
     * @Inject()
     * @var TestModel
     */
    var $testModel;

    /**
     * @return array
     * @throws
     */
    function lists()
    {
        return [time(), 'lists', $this->testModel->getCount()];
    }
}
