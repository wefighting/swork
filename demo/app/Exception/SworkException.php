<?php
namespace Demo\App\Exception;

use Swork\Bean\Annotation\ExceptionHandler;
use Swork\Exception\ExceptionHandlerInterface;
use Swork\Server\Http\Argument;

/**
 * Class SworkException
 * @package Demo\App\Exception
 */
class SworkException extends \Exception
{

}
