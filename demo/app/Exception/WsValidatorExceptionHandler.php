<?php
namespace Demo\App\Exception;

use Swork\Bean\Annotation\ExceptionHandler;
use Swork\Exception\ExceptionHandlerInterface;
use Swork\Exception\WsValidateException;
use Swork\Server\ArgumentInterface;

/**
 * Class AppException
 * @ExceptionHandler(WsValidateException::class)
 * @package Demo\App\Exception
 */
class WsValidatorExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * 处理异常
     * @param ArgumentInterface $argument 当前请求
     * @param \Throwable $ex 异常内容
     * @return mixed
     */
    public function handler(ArgumentInterface $argument, \Throwable $ex)
    {
        return [
            'status' => $ex->getCode(),
            'msg' => $ex->getMessage(),
            'from' => 'WsValidatorExceptionHandler'
        ];
    }
}
