<?php
namespace Demo\App\Exception;

use Swork\Bean\Annotation\DefaultExceptionHandler;
use Swork\Bean\Annotation\ExceptionHandler;
use Swork\Exception\ExceptionHandlerInterface;
use Swork\Server\ArgumentInterface;

/**
 * Class AppException
 * @DefaultExceptionHandler()
 * @ExceptionHandler(SworkException::class)
 * @package Demo\App\Exception
 */
class SworkExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * 处理异常
     * @param ArgumentInterface $argument 当前请求
     * @param \Throwable $ex 异常内容
     * @return mixed
     */
    public function handler(ArgumentInterface $argument, \Throwable $ex)
    {

        return [
            'default' => true,
            'status' => $ex->getCode(),
            'msg' => $ex->getMessage(),

        ];
    }
}
