<?php
namespace Demo\App\Controller;

use Demo\App\Model\UserModel;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/model")
 */
class ModelController extends BeanCollector
{
    /**
     * 不用注释，直接调用表Model进行操作
     * @param Argument $arg
     * @return array
     * @throws
     */
    public function index(Argument $arg)
    {
        return UserModel::M()->getList();
    }
}
