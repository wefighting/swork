<?php
namespace Demo\App\Controller;

use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Client\Amqp;
use Swork\Server\Http\Argument;
use Swork\Bean\Annotation\Inject;

/**
 * Class IndexController
 * @Controller("/amqp")
 */
class AmqpController extends BeanCollector
{
    /**
     * @Inject("send_sms")
     * @var Amqp
     */
    private $amqp;

    /**
     * 测试
     * @param Argument $arg
     * @return mixed
     * @throws
     */
    public function index(Argument $arg)
    {
        //发送内容
        $this->amqp->basePublish('send_sms');

        //返回
        return 'ok';
    }
}
