<?php
namespace Demo\App\Controller;

use Demo\App\Exception\AppException;
use Demo\App\Lib\HttpValidate;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\Method;
use Swork\Bean\Annotation\Validate;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * 请求参数验证器
 * @Controller("/valid")
 */
class ValidController extends BeanCollector
{
    /**
     * 自定义参数检验器
     * @param $v
     */
    public static function check1($v)
    {

    }

    /**
     * 默认是对GET的参数进行校验，可通过Method参数指定为POST校验。
     * @param Argument $argument
     * @Validate(Method::Post)
     * @Validate("name1", Validate::Required)
     * @Validate("name2", Validate::Required, "自定义提示信息")
     * @return array
     */
    public function post(Argument $argument)
    {
        return $argument->post();
    }

    /**
     * 必须输入（只检验值是否为空值）
     * @param Argument $argument
     * @Validate("name", Validate::Required, "自定义提示信息")
     * @return array
     */
    public function required(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查值是否在指定范围内（指定在1，2，3三个值内)
     * @param Argument $argument
     * @Validate("type", Validate::Ins[1|2|3])
     * @return array
     */
    public function ins(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查值是否为纯数字（0-9）
     * @param Argument $argument
     * @Validate("type", Validate::Number)
     * @return array
     */
    public function number(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查值是否为纯字母（不分大小写）
     * @param Argument $argument
     * @Validate("id", Validate::Letter)
     * @return array
     */
    public function letter(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查值是否为字母+数字
     * @param Argument $argument
     * @Validate("id", Validate::LetterNumber)
     * @return array
     */
    public function letterNumber(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查值是否为字母+数字+中线+下线+逗号（A-Za-z0-9_\-,）
     * @param Argument $argument
     * @Validate("name", Validate::TextKey)
     * @return array
     */
    public function text(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查值是否为不等于0
     * @param Argument $argument
     * @Validate("id", Validate::NotZero)
     * @Validate("pwd", Validate::Equal[$pwd2])  // 解析 pwd = pwd2 的值相同？
     * @Validate("num", Validate::Equal[10])     // 解析 num = 10 的规则？
     * @Validate("num", Validate::Greater[10])   // 解析 num > 10 的规则？
     * @return array
     */
    public function NotZero(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查数组长度
     * @param Argument $argument
     * @Validate(Method::Post)
     * @Validate("name", Validate::Required)
     * @Validate("name", Validate::Count[1-])
     * @return array
     */
    public function count(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查字符长度
     * @param Argument $argument
     * @Validate("name", Validate::Length[2-])
     * @Validate("name", Validate::Length[-3])
     * @return array
     */
    public function length(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查数字范围
     * @param Argument $argument
     * @Validate("code", Validate::Range[2-])
     * @Validate("code", Validate::Range[-10])
     * @return array
     */
    public function range(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查邮箱格式
     * @param Argument $argument
     * @Validate("email", Validate::Email)
     * @return array
     */
    public function email(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查手机格式
     * @param Argument $argument
     * @Validate("mobile", Validate::Mobile)
     * @return array
     */
    public function mobile(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查url格式
     * @param Argument $argument
     * @Validate("url", Validate::Url)
     * @return array
     */
    public function url(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查日期格式
     * @param Argument $argument
     * @Validate("date", Validate::Date)
     * @return array
     */
    public function date(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查日期格式
     * @param Argument $argument
     * @Validate("month", Validate::Month)
     * @return array
     */
    public function month(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查时间格式
     * @param Argument $argument
     * @Validate("time", Validate::Time)
     * @return array
     */
    public function time(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查时间格式
     * @param Argument $argument
     * @Validate("time", Validate::DateTime)
     * @return array
     */
    public function datetime(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查时间格式
     * @param Argument $argument
     * @Validate("decimal", Validate::Decimal)
     * @return array
     */
    public function decimal(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查时间格式
     * @param Argument $argument
     * @Validate("digits", Validate::Digits)
     * @return array
     */
    public function digits(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 检查时间格式
     * @param Argument $argument
     * @Validate("pwd", Validate::Equal[12])
     * @Validate("pwd", Validate::Equal[$pwd2])
     * @return array
     */
    public function equal(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 前面字段是否大于后面字段，或固定数值
     * @param Argument $argument
     * @Validate("greater", Validate::Greater[12])
     * @Validate("greater", Validate::Greater[$greater2])
     * @return array
     */
    public function greater(Argument $argument)
    {
        return $argument->query();
    }

    /**
     * 前面字段是否大于等于后面字段，或固定数值
     * @param Argument $argument
     * @Validate("greater", Validate::GreaterEqual[12])
     * @Validate("greater", Validate::GreaterEqual[$greater2])
     * @return array
     */
    public function greaterEqual(Argument $argument)
    {
        return $argument->query();
    }

    /**
     *  前面字段是否小于后面字段，或固定数值
     * @param Argument $argument
     * @Validate("lesser", Validate::Lesser[12])
     * @Validate("lesser", Validate::Lesser[$lesser2])
     * @return array
     */
    public function lesser(Argument $argument)
    {
        return $argument->query();
    }

    /**
     *  前面字段是否小于等于后面字段，或固定数值
     * @param Argument $argument
     * @Validate("lesser", Validate::lesserEqual[12])
     * @Validate("lesser", Validate::lesserEqual[$lesser2])
     * @return array
     */
    public function lesserEqual(Argument $argument)
    {
        return $argument->query();
    }

}
