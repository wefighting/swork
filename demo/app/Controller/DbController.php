<?php
namespace Demo\App\Controller;

use Demo\App\Model\SortModel;
use Demo\App\Model\TestModel;
use Demo\App\Model\UserModel;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/db")
 */
class DbController extends BeanCollector
{
    /**
     * @Inject()
     * @var TestModel
     */
    private $testModel;

    /**
     * @Inject()
     * @var UserModel
     */
    private $userModel;

    /**
     * @Inject()
     * @var SortModel
     */
    private $sortModel;

    /**
     * 测试当主键重复时自动更新
     * @param Argument $arg
     * @return mixed
     */
    public function insert(Argument $arg)
    {
        $data = [
            'id' => 2,
            'name' => 'username',
            'atime' => time()
        ];
        return $this->testModel->insert($data, true);
    }

    /**
     * 通过条件更新
     * @param Argument $arg
     * @return mixed
     */
    public function update1(Argument $arg)
    {
        $data = [
            'tname' => 'username',
        ];
        $direct = [
            'utime' => 'atime',
        ];
        return $this->testModel->update(['tid' => 1], $data, $direct);
    }

    /**
     * 通过条件更新
     * @param Argument $arg
     * @return mixed
     */
    public function index1(Argument $arg)
    {
        $data = [
            'tname' => 'username',
        ];
        $where = [
            'tid' => 1,
            '$index' => 'idx1'
        ];
        return $this->testModel->update($where, $data);
    }

    /**
     * 通过条件更新
     * @param Argument $arg
     * @return mixed
     */
    public function index2(Argument $arg)
    {
        $where = [
            'tid' => 1,
            '$index' => 'idx1'
        ];
        return $this->testModel->getList($where);
    }

    /**
     * 通过条件更新
     * @param Argument $arg
     * @return mixed
     */
    public function index3(Argument $arg)
    {
        $where = [
            'tid' => 0,
            '$index' => 'idx1'
        ];
        return $this->testModel->delete($where);
    }

    public function join1(Argument $arg)
    {
        return $this->testModel
            ->join($this->userModel, ['uid' => 'id'])
            ->getList(['sid' => 2]);
    }

    public function join2(Argument $arg)
    {
        return $this->testModel
            ->join($this->userModel, ['uid' => 'id'])
            ->join($this->sortModel, ['sid' => 'sid'])
            ->getList(['C.sid' => 2]);
    }

    public function join3(Argument $arg)
    {
        return $this->testModel
            ->join($this->userModel, ['uid' => 'id'])
            ->join($this->sortModel, ['sid' => 'sid'])
            ->getRow(['C.sid' => 2]);
    }

    public function join4(Argument $arg)
    {
        return $this->testModel
            ->join($this->userModel, ['uid' => 'id'])
            ->join($this->sortModel, ['sid' => 'sid'])
            ->getCount(['C.sid' => 2]);
    }

    public function join5(Argument $arg)
    {
        return $this->testModel
            ->leftJoin($this->userModel, ['uid' => 'id'])
            ->leftJoin($this->sortModel, ['sid' => 'sid'])
            ->getList();
    }

    public function having(Argument $arg)
    {
        $where = [
            '$group' => ['sex'],
            '$having' => [
                ':count' => 2,
                'age' => ['>' => 20]
            ]
        ];
        return $this->userModel->getList($where, 'sex,age,count(*) as count');
    }
}
