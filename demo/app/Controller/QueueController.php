<?php
namespace Demo\App\Controller;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Queue;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/queue")
 */
class QueueController extends BeanCollector
{
    public function push1(Argument $args)
    {
        Queue::push('testA', ['oid' => microtime(true)], 2);
        return ['ok'];
    }

    public function pop1(Argument $args)
    {
        $pop = Queue::pop('testA');
        if ($pop != false)
        {
            Queue::ack('testA', $pop['id']);
        }
        return $pop;
    }

    public function push2(Argument $args)
    {
        Queue::push('testB', ['pid' => microtime(true)], 1);

        return ['ok'];
    }

    public function pop2(Argument $argument)
    {
        $pop =  Queue::pop('testB');
        if ($pop != false)
        {
            Queue::ack('testB', $pop['id']);
        }
        return $pop;
    }
}
