<?php
namespace Demo\App\Controller\Db;

use Demo\App\Model\SortModel;
use Demo\App\Model\TestModel;
use Demo\App\Model\UserModel;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/db/query")
 */
class QueryController extends BeanCollector
{
    /**
     * 直接输出
     * @param Argument $arg
     * @return mixed
     */
    public function in1(Argument $arg)
    {
        $ids = [
            ['tid' => 1],
            ['tid' => 3],
            ['tid' => 5],
        ];
        return TestModel::M()->getList(['tid' => $ids]);
    }

    /**
     * 直接输出
     * @param Argument $arg
     * @return mixed
     */
    public function in2(Argument $arg)
    {
        $ids = [1, 3];
        $list = TestModel::M()->getList(['tid' => $ids]);
        return $list;
    }

    /**
     * 直接输出
     * @param Argument $arg
     * @return mixed
     * @throws
     */
    public function join1(Argument $arg)
    {

        $where = [
            'C.sid' => 2,
            'B.age' => ['>' => 2],
            '$index' => 'A.idx_test_uid,B.idx2,C.idx_sort_atime,B.idx_user_age'
        ];
        return TestModel::M()
            ->join(UserModel::M(), ['uid' => 'id'])
            ->join(SortModel::M(), ['sid' => 'sid'])
            ->getList($where, 'A.*,B.age');
    }

    /**
     * 直接输出
     * @param Argument $arg
     * @return mixed
     */
    public function join2(Argument $arg)
    {
        $where = [
            'B.id' => 2,
            'B.age' => ['>' => 0],
            '$index' => 'A.idx_test_uid'
        ];
        return TestModel::M()
            ->join(UserModel::M(), ['uid' => 'id'])
            ->selectAS([])
            ->getList($where);
    }

    /**
     * 子查询输出
     * @param Argument $arg
     * @return mixed
     */
    public function joinas1(Argument $arg)
    {
        $where = [
            'A.age' => ['>' => 20],
        ];
        return UserModel::M()
            ->joinAS(UserModel::M(), ['id' => 'id'], [], '*', [], 10, 1)
            ->straightJoinAS(TestModel::M(), ['id' => 'uid'])
            ->selectAS([':atime' => ['>' => 0]])
            ->getList($where, 'A.*');
    }
}
