<?php
namespace Demo\App\Controller\Db;

use Demo\App\Model\SortModel;
use Demo\App\Model\TestModel;
use Demo\App\Model\UserModel;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/db/distinct")
 */
class DistinctController extends BeanCollector
{
    /**
     * @Inject()
     * @var TestModel
     */
    private $testModel;

    /**
     * @Inject()
     * @var UserModel
     */
    private $userModel;

    /**
     * @Inject()
     * @var SortModel
     */
    private $sortModel;

    /**
     * 直接输出
     * @param Argument $arg
     * @return mixed
     */
    public function direct(Argument $arg)
    {
        return $this->testModel->getDistinct('tname', []);
    }

    /**
     * 通过JOIN表输出，并指定从哪个集合输出
     * @param Argument $arg
     * @return mixed
     */
    public function join(Argument $arg)
    {
        return $this->testModel
            ->leftJoin($this->userModel, ['uid' => 'id'])
            ->leftJoin($this->sortModel, ['sid' => 'sid'])
            ->getDistinct('A.tname');
    }
}
