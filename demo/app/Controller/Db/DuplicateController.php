<?php
namespace Demo\App\Controller\Db;

use Demo\App\Model\TestModel;
use Demo\App\Model\UserModel;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/db/duplicate")
 */
class DuplicateController extends BeanCollector
{
    /**
     * 单条插入
     * @param Argument $arg
     * @return mixed
     */
    public function insert(Argument $arg)
    {
        $data = [
            'id' => 4,
            'name' => 'user4',
            'desc' => 'user4-desc',
            'sex' => 1,
            'age' => 20,
            'atime' => time()
        ];
        return UserModel::M()->insert($data, true);
    }

    /**
     * 批量插入
     * @param Argument $arg
     * @return mixed
     */
    public function inserts(Argument $arg)
    {
        $data = [
            [
                'id' => 4,
                'name' => 'user4',
                'desc' => 'user4-desc',
                'sex' => 1,
                'age' => 20,
                'atime' => time()
            ],
            [
                'id' => 5,
                'name' => 'user4',
                'desc' => 'user4-desc',
                'sex' => 1,
                'age' => 20,
                'atime' => time()
            ],
            [
                'id' => 6,
                'name' => 'user4',
                'desc' => 'user4-desc',
                'sex' => 1,
                'age' => 20,
                'atime' => time()
            ],
            [
                'id' => 7,
                'name' => 'user4',
                'desc' => 'user4-desc',
                'sex' => 1,
                'age' => 20,
                'atime' => time()
            ],
        ];
        return UserModel::M()->inserts($data, true);
    }
}
