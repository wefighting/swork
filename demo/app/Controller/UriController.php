<?php
namespace Demo\App\Controller;

use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\View;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * /oppo
 * /vivo
 * /imac/oppo
 * /imac/vivo
 * @Controller("/uri")
 */
class UriController extends BeanCollector
{
    public function test1(Argument $arg)
    {
        return 'test1';
    }

    public function test2(Argument $arg)
    {
        return 'test2';
    }

    /**
     * @Controller("/:foo")
     * @View(/web/uri.tpl)
     * @param Argument $arg
     * @return array
     */
    public function index(Argument $arg)
    {
        //$far = $arg->params('far');

        return [
            'title' => 'index' . $arg->param('foo')
        ];
    }

    /**
     * @Controller("/:foo/:bar")
     * @View(/web/uri.tpl)
     * @param Argument $arg
     * @return array
     */
    public function bar(Argument $arg)
    {
        //$far = $arg->params('far');

        return [
            'title' => 'foo-' . $arg->param('foo') . ' |' . $arg->param('bar')
        ];
    }
}
