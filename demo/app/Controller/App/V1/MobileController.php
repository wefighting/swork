<?php
namespace Demo\App\Controller\App\V1;

use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\Validate;

/**
 *
 * @Controller("/app/mobile/v1")
 */
class MobileController extends BeanCollector
{
    /**
     * 发送验证码
     * 通过手机号发送短信验证码
     * @param Argument $argument
     * @Validate(Method::Post)
     * @Validate(Method::Get)
     * @Validate("mobile", Validate::Required)
     * @Validate("mobile", Validate::Mobile)
     * @return array
     * @throws
     */
    public function sendcode(Argument $argument)
    {
        /**
         * 手机号码
         * @var string
         * @require
         * @sample 13955553333
         */
        $mobile = $argument->get('mobile', '');

        /**
         * xxxx
         * @var string
         * @sample 156448939
         */
        $time = $argument->get('time', '');


        //API返回
        /**
        {
            "oid": "", //订单ID
            "rcMsg": "string",
            "rcTime": 1551867341619
        }
         */
        return [
            "oid" => "", //订单ID
            "rcMsg" => "string",
            "rcTime" => 1551867341619
        ];
    }
}
