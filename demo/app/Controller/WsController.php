<?php
namespace Demo\App\Controller;

use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\Validate;
use Swork\Bean\BeanCollector;
use Swork\Server\Ws\WsArgument;

/**
 * WebSocket入口
 * @Controller("/ws")
 */
class WsController extends BeanCollector
{
    public function index(WsArgument $argument)
    {
        return ['src' => 'index'];
    }

    public function open(WsArgument $argument)
    {
        var_dump('open');
        //$argument->setBreak();
        return ['time' => time()];
    }

    public function close(WsArgument $argument)
    {
        var_dump('close');
    }

    /**
     * @Validate("name", Validate::Required)
     * @param WsArgument $argument
     * @return string
     */
    public function test(WsArgument $argument)
    {
        var_dump($argument->get());
        var_dump('test');
        return 'test';
    }
}
