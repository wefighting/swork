<?php
namespace Demo\App\Controller;

use Demo\App\Exception\AppException;
use Demo\App\Logic\TestLogic;
use Demo\App\Middleware\ClassAfterMiddleware;
use Demo\App\Middleware\ClassBeforeMiddleware;
use Demo\App\Middleware\MethodAfterMiddleware;
use Demo\App\Middleware\MethodBeforeMiddleware;
use Demo\App\Model\TestModel;
use Demo\App\Model\UserModel;
use Demo\App\Service\IdInterface;
use Demo\App\Service\TestInterface;
use Demo\App\Task\TestTask;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\Annotation\Middleware;
use Swork\Bean\Annotation\Reference;
use Swork\Bean\BeanCollector;
use Swork\Client\MySql;
use Swork\Context;
use Swork\Db\Db;
use Swork\Exception\DbException;
use Swork\Server\Http\Argument;
use Swork\Server\Tasker;
use Swork\Service;

/**
 * Class IndexController
 * @Controller("/")
 * @Middleware(ClassBeforeMiddleware::class)
 * @Middleware(ClassAfterMiddleware::class)
 */
class IndexController extends BeanCollector
{
    /**
     * @Inject()
     * @var TestLogic
     */
    private $testLogic;

    /**
     * @Inject()
     * @var TestModel
     */
    private $testModel;

    /**
     * @Inject()
     * @var UserModel
     */
    private $userModel;

    /**
     * @Reference()
     * @var TestInterface
     */
    private $testService;

    /**
     * @Inject()
     * @var MySql
     */
    private $mysql;

    public function index(Argument $arg)
    {
        foreach ([1, 2] as $value)
        {
            echo $value . PHP_EOL;
        }
        return [uniqid(), time(), microtime(), date('Ymd')];
    }

    /**
     * 测试
     * @param Argument $args
     * @return mixed
     * @throws
     */
    public function test(Argument $args)
    {
        try
        {
//            Db::beginTransaction();
//            $this->userModel->insert(['age' => 2002]);
//            $this->userModel->insert(['age' => 22121]);
//            Db::rollback();
            $count = $this->userModel->getCount();
            Service::$logger->alert('lalalllallalla', ['fdafhdahfjkdajfda', 'fdafhdkafhdjakhdjsk']);
        }
        catch (DbException $e)
        {

        }
        $output = 'output >>> ' . time();
        echo $output . PHP_EOL;
        return $output;
    }

    /**
     * @return array
     * @Middleware(MethodBeforeMiddleware::class)
     * @Middleware(MethodAfterMiddleware::class)
     * @throws
     */
    public function info(Argument $arg)
    {
        $count = 0;// $this->testLogic->getCount();
        return ['status' => 200, 'data' => time(), 'count' => $count];
    }

    public function sqlquery()
    {
        $rel1 = $this->mysql->query('select * from user limit 2');
        $rel2 = $this->mysql->query('delete from `user` where `id`=3413');
        $rel3 = $this->mysql->query('update `user` set `name`=\'update 111\' where `id`=3415');
        $rel4 = $this->mysql->query('select count(*) as count from user');
        $rel5 = $this->mysql->query('select sex,sum(age) as age from user group by sex');
        return [$rel1, $rel2, $rel3, $rel4, $rel5];
    }

    public function dict()
    {
        return $this->testModel->getDict('sex');
    }

    public function logic()
    {
        $insert = $this->testLogic->insert();
        $row = $this->testLogic->getRow();
        $list = $this->testLogic->getList();
        $count = $this->testLogic->getCount();
        $delete = $this->testLogic->delete();

        return [
            'insert' => $insert,
            'row' => $row,
            'list' => $list,
            'count' => $count,
            'delete' => $delete,
        ];
    }

    /**
     * @return array|bool
     */
    public function insert()
    {
        $rel1 = $this->testModel->insert([
            'name' => 'deiva',
            'desc' => 'insert1',
            'sex' => 1,
            'missing' => 'ng'
        ]);
        $rel2 = $this->testModel->insert([
            'name' => 'liang',
            'desc' => 'insert2',
            'age' => 30,
        ]);
        return [$rel1, $rel2];
    }

    /**
     * @return array|bool
     * @throws
     */
    public function inserts()
    {
        $datas = [
            [
                'name' => 'deiva',
                'desc' => 'insert1',
                'sex' => 1,
                'missing' => 'ng'
            ],
            [
                'name' => 'liang',
                'desc' => 'insert2',
                'age' => 30,
            ]
        ];
        $rel = $this->testModel->inserts($datas);
        return [$rel];
    }

    public function model()
    {
        $update1 = $this->testModel->updateById(3214, ['name' => '222222222']);
        $update2 = $this->testModel->updateById(3526, ['name' => time()]);
        $delete = $this->testModel->deleteById(3212);

        return [
            'update1' => $update1,
            'update2' => $update2,
            'delete' =>  $delete
        ];
    }

    public function rpc()
    {
        return [
            $this->testService->mysql()
        ];
    }

    public function service()
    {
        return $this->testService->info(10, 20);
    }

    public function deliver()
    {
        Tasker::deliver(TestTask::class, 'test03');
    }
}
