<?php
namespace Demo\App\Controller\Helper;

use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Helper\NumberHelper;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/helper/number")
 */
class NumberController extends BeanCollector
{
    /**
     * 直接输出
     * @param Argument $arg
     * @return mixed
     */
    public function percent(Argument $arg)
    {
        return [
            'CalculatePercent1' => NumberHelper::CalculatePercent(21, 891),
            'CalculatePercent2' => NumberHelper::CalculatePercent(20, 1000),
            'CalculatePerThousand1' => NumberHelper::CalculatePerThousand(39, 1332),
            'CalculatePerThousand2' => NumberHelper::CalculatePerThousand(40, 2000),
            'CalculateTenThousand1' => NumberHelper::CalculateTenThousand(311,343232),
            'CalculateTenThousand2' => NumberHelper::CalculateTenThousand(300,400000),
            'CalculateTenThousand3' => NumberHelper::CalculateTenThousand(0,400000),
            'CalculateTenThousand4' => NumberHelper::CalculateTenThousand(300,0),
        ];
    }
}
