<?php
namespace Demo\App\Controller\Tcp;

use Demo\App\Model\TestModel;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\Annotation\TcpListener;
use Swork\Bean\BeanCollector;
use Swork\Server\Tcp\TcpArgument;

/**
 * Class TestTcpController
 * @TcpListener("/test/tcp")
 */
class TestTcpListener extends BeanCollector
{
    /**
     * @Inject()
     * @var TestModel
     */
    private $testModel;

    /**
     * 测试当主键重复时自动更新
     * @param TcpArgument $arg
     * @return mixed
     */
    public function ws(TcpArgument $arg)
    {
//        $data = [
//            'id' => 2,
//            'name' => 'username',
//            'atime' => time()
//        ];
//        return $this->testModel->insert($data, true);
        return null;
    }
}
