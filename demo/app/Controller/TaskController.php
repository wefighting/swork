<?php
namespace Demo\App\Controller;

use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;
use Swork\Server\Tasker;

/**
 * Class IndexController
 * @Controller("/task")
 */
class TaskController extends BeanCollector
{
    /**
     * 测试
     * @param Argument $args
     * @return mixed
     * @throws
     */
    public function index(Argument $args)
    {
        Tasker::deliver(static::class, 'calc', [12, 5]);
        return ['ok'];
    }

    public function calc(int $a, int $b)
    {
        echo "a: $a  -- b：$b" . PHP_EOL;
        Tasker::deliver(static::class, 'child', [[1, 2, 3]]);
    }

    public function delay(Argument $argument)
    {
        Tasker::deliver(static::class, 'child', [[1, 2, 3]], 0, 2);
        return ['ok'];
    }

    public function child(array $arg)
    {
        var_dump($arg);
    }
}
