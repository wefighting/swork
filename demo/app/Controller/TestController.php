<?php
namespace Demo\App\Controller;

use Swoole\Coroutine;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\Annotation\Reference;
use Swork\Bean\BeanCollector;
use Swork\Client\Redis;
use Swork\Db\Db;
use Swork\Helper\DateHelper;
use Swork\Logger\AbstractHandler;
use Swork\Server\Http\Argument;
use Swork\Server\Tasker;
use Swork\Service;

/**
 * Class IndexController
 * @Controller("/test")
 */
class TestController extends BeanCollector
{
    /**
     * @Inject()
     * @var Redis
     */
    public $redis;

    /**
     * @Reference("user")
     * @var \Demo\App\Service\TestInterface
     */
    private $testInterface;

    /**
     * @Inject()
     * @var \Demo\App\Model\TestModel
     */
    private $testModel;

    /**
     * 测试
     * @param Argument $arg
     * @return mixed
     * @throws
     */
    public function index(Argument $arg)
    {
        //$result = $this->testModel->increaseById(1, ['age' => -5]);
        //var_dump($result);
        return 'result2';
    }

    /**
     * mysql测试
     * @param Argument $args
     * @return mixed
     * @throws
     */
    public function mysql(Argument $args)
    {
        $result = $this->testModel->getRow(['id' => '' ?: 0]);
        var_dump($result);
        //        $result = $this->testModel->query("update user set name='fengwanqing' where id=1");
        //        var_dump($result);
        //        $this->testModel->insert([
        //            'age' => 29,
        //            'name' => IdHelper::generate(),
        //            'sex' => rand(1, 100),
        //            'desc' => IdHelper::generate()
        //        ]);
        ////        $result = $this->testInterface->mysql();
        ////        $result = $this->testInterface->mysql();
        ////        var_dump($result);
        ////        return $result;
        //        $result = $this->testModel->doQuery('select sleep(0);');
        ////        Coroutine::sleep(3);
        //        $result = $this->testModel->getRow([
        ////            'id' => rand(1, 10000),
        ////            'name' => IdHelper::generate()
        //        ], 'id,name');
        //        var_dump($result);
        //        var_dump($result);
        //        $result = $this->testModel->update(['id' => rand(1, 1000)], ['name' => IdHelper::generate(), 'age' => 1]);
        return 'debug';
        //        return $result;
    }


    /**
     * mysql测试
     * @param Argument $args
     * @return mixed
     * @throws
     */
    public function mysql2(Argument $args)
    {
        //        try
        //        {
        var_dump('cid >>> ' . Coroutine::getuid());
        Db::beginTransaction();
        //            $this->testModel->insert([
        //                'age' => rand(1, 200),
        //                'name' => IdHelper::generate(),
        //                'sex' => rand(0, 1),
        //                'desc' => IdHelper::generate()
        //            ]);

        $result = $this->testModel->getRow([
            'id' => rand(1, 10000),
            //            'name' => IdHelper::generate()
        ], 'id,name');
        $this->testModel->doQuery('select sleep(10);');
        //            Db::rollback();
        //            Db::commit();
        //        }
        //        catch (\Throwable $e)
        //        {
        //            Db::rollback();
        //        }

        return 'mysql2';
    }

    /**
     * redis测试
     * @param Argument $args
     * @return mixed
     * @throws
     */
    public function redis(Argument $args)
    {
        //        $result = $this->testInterface->redis();
        //        var_dump($result);
        $result = $this->redis->set('key', 'value');
        return $result;
    }

    /**
     * logger测试
     * @param Argument $args
     * @return mixed
     * @throws
     */
    public function logger(Argument $args)
    {
        Service::$logger->error('日志输出%s', ['lalala', 'dafdafda', 'ccccc', 'ddddd']);
        return 'logger';
    }

    /**
     * Task测试
     * @param Argument $args
     * @return mixed
     * @throws
     */
    public function deliver(Argument $args)
    {
        Tasker::deliver(TestController::class, 'deliverTask');
        return 'deliver';
    }

    public function deliverTask()
    {
        $result = $this->testModel->update(['id' => 1], ['name' => 'fengwanqing']);
        var_dump($result);
        //        $result = $this->testModel->getRow(['id' => 127]);
        //        $result = $this->testModel->updateById(81, [
        //            'name' => 'zhouxueqing1',
        //            'age' => 20,
        //            'sex' => 1,
        //            'desc' => 'bitiful'
        //        ]);
        //        var_dump($result);
    }

    public function deliverTask2()
    {
        $this->testModel->query('select sleep(10);');
        $result = $this->testModel->getRow();
        var_dump('name2 >>> ' . $result['name'] . ', time2 >>> ' . DateHelper::getTime());
    }

    public function cutfile()
    {
        $a = new AbstractHandler();
        $a->cutfile();
    }
}
