<?php
namespace Demo\App\Controller;

use Demo\App\Exception\AppException;
use Demo\App\Logic\TestLogic;
use Demo\App\Middleware\ClassAfterMiddleware;
use Demo\App\Middleware\ClassBeforeMiddleware;
use Demo\App\Middleware\MethodAfterMiddleware;
use Demo\App\Middleware\MethodBeforeMiddleware;
use Demo\App\Model\TestModel;
use Demo\App\Model\UserModel;
use Demo\App\Service\TestInterface;
use Demo\App\Task\TestTask;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\Annotation\Middleware;
use Swork\Bean\Annotation\Reference;
use Swork\Bean\BeanCollector;
use Swork\Client\MySql;
use Swork\Context;
use Swork\Db\Db;
use Swork\Exception\DbException;
use Swork\Server\Http\Argument;
use Swork\Server\Tasker;
use Swork\Service;

/**
 * Class FileController
 * @Controller("/file")
 */
class FileController extends BeanCollector
{

    /**
     * 测试
     * @param Argument $args
     * @return mixed
     * @throws
     */
    public function index(Argument $args)
    {
        $args->sendfile(Service::$env['root'] . 'runtime' . Service::$env['sep'] . 'swork.log', 'export.log');
        return '';
    }
}
