<?php
namespace Demo\App\Breaker;

use Demo\App\Exception\AppException;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\BeanCollector;
use Swork\Client\Redis;
use Swork\Breaker\BreakerInterface;

/**
 * Class HttpGetBreaker
 * @package Demo\App\Task
 */
class HttpGetBreaker extends BeanCollector implements BreakerInterface
{
    /**
     * @Inject()
     * @var Redis
     */
    public $redis;

    /**
     * 计算时间周期
     * @var int
     */
    public $cycleTime = 10000;

    /**
     * 周期内最大请求数量
     * @var int
     */
    public $maxCount = 1;

    /**
     * 达到最大周期后熔断持续时间
     * @var int
     */
    public $duration = 100000;

    /**
     * 生成计算周期的标识符（为空表示所有请求）
     * @return string
     */
    public function getIdentifier()
    {
        return 'abcd';
    }

    /**
     * 断开时执行
     * @param string $identifier 标识符
     * @return mixed|void
     * @throws
     */
    public function handleBreak(string $identifier)
    {
//        throw new AppException('截断');
    }

    /**
     * 取出最后记录时间
     * @param string $identifier
     * @return float
     */
    public function fetchLastTime(string $identifier)
    {
        $val = $this->redis->get('break_lt_' . $identifier);
        return floatval($val);
    }

    /**
     * 记录最后记录时间
     * @param string $identifier
     * @param float $time
     * @return bool|mixed
     */
    public function putLastTime(string $identifier, float $time)
    {
        return $this->redis->set('break_lt_' . $identifier, $time);
    }

    /**
     * 取出数量
     * @param string $identifier
     * @return int
     */
    public function fetchCount(string $identifier)
    {
        $val = $this->redis->get('break_c_' . $identifier);
        return intval($val);
    }

    /**
     * 记录数量
     * @param string $identifier
     * @param int $count
     * @return bool|mixed
     */
    public function putCount(string $identifier, int $count)
    {
        return $this->redis->set('break_c_' . $identifier, $count);
    }

    /**
     * 取出最后截断时间
     * @param string $identifier
     * @return float
     */
    public function fetchLastBreakTime(string $identifier)
    {
        $val = $this->redis->get('break_lbt_' . $identifier);
        return floatval($val);
    }

    /**
     * 记录最后截断时间
     * @param string $identifier
     * @param float $time
     * @return bool|mixed
     */
    public function putLastBreakTime(string $identifier, float $time)
    {
        return $this->redis->set('break_lbt_' . $identifier, $time);
    }
}
