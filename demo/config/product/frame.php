<?php
/**
 * 框架配置
 * @var array
 */
return [
    //命名空间前缀
    'ns_prefix' => 'Demo\\App\\',

    //视图路径
    'view_dir' => 'app/View',
];
