<?php
return [
    'default' => [
        'uri' => [
            'host' => '193.112.240.108',
            'port' => 4000,
            'user' => 'root',
            'password' => 'Getmind1107',
            'database' => 'test',
            'charset' => 'utf8',
            'timeout' => 0,
            'filter' => \Demo\App\Behavior\SqlFilter::class,
            'recorder' => \Demo\App\Behavior\SqlRecorder::class,
            'sqlManualInter' => false,
        ],
        //'read' => 'default_read',
        'pools' => 50,
    ],
    'default_read' => [
        'uri' => [
            'host' => '127.0.0.1',
            'port' => 3306,
            'user' => 'root',
            'password' => '123456',
            'database' => 'test_read',
            'charset' => 'utf8',
            'timeout' => 0,
            'filter' => \Demo\App\Behavior\SqlFilter::class,
            'recorder' => \Demo\App\Behavior\SqlRecorder::class,
            'sqlManualInter' => false,
        ],
        'pools' => 50,
    ],

    'inc2' => [
        'uri' => [
            'host' => '127.0.0.1',
            'port' => 3306,
            'user' => 'root',
            'password' => '123456',
            'database' => 'test',
            'charset' => 'utf8mb4',
            'timeout' => 0,
            'filter' => \Demo\App\Behavior\SqlFilter::class,
            'recorder' => \Demo\App\Behavior\SqlRecorder::class
        ],
        'pools' => 50,
    ],
];
