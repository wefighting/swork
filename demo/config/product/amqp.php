<?php
return [
    'uri' => [
        'host' => '127.0.0.1',
        'port' => 5672,
        'user' => 'guest',
        'password' => 'guest'
    ],
    'pools' => 50,
    'enable' => true,
    'threads' => true,
    'consumers' => [
        \Demo\App\Amqp\TestAmqp::class => [
            'test01' => 3,
            'test02' => 2,
        ],
        \Demo\App\Amqp\TestAmqp2::class => [
            'test01' => true,
        ],
    ],
];
