<?php
return [
    'uri' => [
        'host' => '127.0.0.1',
        'port' => 6379,
        'password' => ''
    ],
    'pools' => 50,
    'prefix' => 'swork_'
];
