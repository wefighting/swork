<?php
return [
    'httpserver' => [
        'host' => '0.0.0.0',
        'port' => 8199
    ],
    'tcpserver' => [
        'host' => '0.0.0.0',
        'port' => 8099
    ],
    'worker_num' => 1,
    'task_worker_num' => 2,
    'task_timeout' => 10,
    'timer_task' => true,
    'cluster_srvs' => [],
    'auto_reload' => true,
    'scan_handler' => \Demo\App\Handler\ScanHandler::class,
    'error_handler' => \Demo\App\Exception\SworkException::class,
    'table_size' => 51200,
    'table_col_data_size' => 256,
    'table_col_num_size' => 4,
    'table_col_items' => [
        'data1' => ['type' => \swoole_table::TYPE_STRING, 'size' => 256],
        'num2' => ['type' => \swoole_table::TYPE_INT, 'size' => 4],
    ]
];
