<?php
return [

    'default' => [
        'uri' => [
            'host' => '127.0.0.1',
            'port' => 3306,
            'user' => 'root',
            'password' => '123456',
            'database' => 'test',
            'charset' => 'utf8',
        ],
        'pools' => 50,
    ],

    'inc2' => [
        'uri' => [
            'host' => '127.0.0.1',
            'port' => 3306,
            'user' => 'root',
            'password' => '123456',
            'database' => 'test',
            'charset' => 'utf8',
        ],
        'pools' => 50,
    ],
];
