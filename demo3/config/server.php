<?php
return [
    'httpserver' => [
        'host' => '0.0.0.0',
        'port' => 6199
    ],
    'tcpserver' => [
        'host' => '0.0.0.0',
        'port' => 6099
    ],
    'worker_num' => 1,
    'task_worker_num' => 1,
    'timer_task' => true,
    'cluster_task' => true,
    'cluster_srvs' => ['127.0.0.1:8099','127.0.0.1:7099','127.0.0.1:6099'],
    'auto_reload' => false,
    'error_handler' => \Demo3\App\Exception\SworkException::class
];
