<?php
namespace Demo3\App\Exception;

use Swork\Server\Http\Argument;

class SworkException
{
    public function handler(Argument $argument, \Throwable $ex)
    {
        return [
            'status' => $ex->getCode(),
            'msg' => $ex->getMessage()
        ];
    }
}
