<?php
namespace Swork\Middleware;

use Swork\Server\ArgumentInterface;

interface BeforeMiddlewareInterface
{
    /**
     * 中间件处理层（请求前的处理）
     * @param ArgumentInterface $argument 请求参数
     * @rerturn bool
     */
    public function process(ArgumentInterface $argument);
}
