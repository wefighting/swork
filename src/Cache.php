<?php
namespace Swork;

use Swork\Cache\CacheInterface;
use Swork\Cache\RedisCache;

/**
 * 系统缓存组件
 * Class Cache
 * @package Swork
 */
class Cache
{
    /**
     * 接口实例化
     * @var CacheInterface
     */
    private static $_instance = null;

    /**
     * 根据KEY删除缓存
     * @param string $key 根据KEY获取缓存
     * @return mixed
     */
    public static function del(string $key)
    {
        if (self::$_instance == null)
        {
            self::$_instance = new RedisCache();
        }
        return self::$_instance->del($key);
    }

    /**
     * 根据KEY获取缓存
     * @param string $key 根据KEY获取缓存
     * @return mixed
     */
    public static function get(string $key)
    {
        if (self::$_instance == null)
        {
            self::$_instance = new RedisCache();
        }
        return self::$_instance->get($key);
    }

    /**
     * 设置缓存内容
     * @param string $key
     * @param mixed $value 要缓存的内容
     * @param int $timeout 超时时间（单位秒）
     * @return mixed
     */
    public static function set(string $key, $value, int $timeout = 0)
    {
        if (self::$_instance == null)
        {
            self::$_instance = new RedisCache();
        }
        return self::$_instance->set($key, $value, $timeout);
    }
}


