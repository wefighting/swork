<?php
namespace Swork\Queue;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * RabbitMQ队列参数对象
 * Class AmqpArgument
 * @package Swork\Server\Http
 */
class AmqpArgument
{
    /**
     * server
     * @var \swoole_websocket_server
     */
    private $server;

    /**
     * 用户简易路径的参数
     * @var AMQPChannel
     */
    private $channel;

    /**
     * @var AMQPMessage
     */
    private $message;

    /**
     * Argument constructor.
     * @param \swoole_websocket_server $serve
     * @param AMQPChannel $channel
     * @param AMQPMessage $message
     */
    public function __construct(\swoole_websocket_server $server, AMQPChannel $channel, AMQPMessage $message)
    {
        $this->server = $server;
        $this->channel = $channel;
        $this->message = $message;
    }

    /**
     * 获取通道
     * @return AMQPChannel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * 获取消息
     * @return AMQPMessage $message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * 获取消息内容
     * @return string
     */
    public function getBody()
    {
        return $this->message->getBody();
    }
}
