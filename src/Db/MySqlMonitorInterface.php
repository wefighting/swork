<?php
namespace Swork\Db;

interface MySqlMonitorInterface
{
    /**
     * 注册插入数据监听
     * @return array|bool
     */
    public function registerInsertMonitor();

    /**
     * 注册更新数据监听
     * @return array|bool
     */
    public function registerUpdateMonitor();

    /**
     * 监听插入数据回调
     * @param mixed $keyId 监控的回传主键ID
     * @param array $data 监控的回传数据
     */
    public function insertMonitorCallback($keyId, array $data);

    /**
     * 监听更新数据回调
     * @param array $where 当时运行的数据条件
     * @param array $data 监控的回传数据
     */
    public function updateMonitorCallback(array $where, array $data);
}
