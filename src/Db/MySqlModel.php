<?php
namespace Swork\Db;

use Swork\Bean\Holder\InstanceHolder;
use Swork\Client\MySql;
use Swork\Exception\DbException;
use Swork\Server\Tasker;

class MySqlModel extends MySql
{
    /**
     * 自增主键
     */
    const AutoKeyID = 1;

    /**
     * 自增主键
     */
    const KeyID = 2;

    /**
     * 表名
     * @var string
     */
    private $tbl;

    /**
     * 主键信息（哪个是主键和主键类型）
     * @var array
     */
    private $key;

    /**
     * 字段结构（声明默认值）
     * @var array
     */
    private $cols;

    /**
     * 默认对象
     * @var mixed
     */
    private $default;

    /**
     * 监控对象
     * @var MySqlMonitorInterface
     */
    private $monitor;

    /**
     * 连接节点
     * @var string
     */
    private $node;

    /**
     * 当前实例化对象
     * @var MySqlModel
     */
    private static $instace = null;

    /**
     * 转换成原始数据类型
     * @var bool
     */
    private $forceToDataType = false;

    /**
     * 子查询配置
     * @var array
     */
    private $selectAS;

    /**
     * 初始表结构
     * @param string $tbl 表名
     * @param array $key 主键信息（哪个是主键和主键类型）
     * @param array $cols 字段结构（声明默认值）
     * @param string $node 使用连接数据库节点
     * @throws
     */
    public function __construct(string $tbl, array $key, array $cols, string $node)
    {
        //参数赋值
        $this->tbl = $tbl;
        $this->key = $key;
        $this->cols = $cols;
        $this->node = $node;
        $this->getCollector($node);

        //判断是否实现监控接口
        $rc = new \ReflectionClass(static::class);
        $ifs = $rc->getInterfaces();
        if (isset($ifs[MySqlMonitorInterface::class]))
        {
            $this->monitor = &$this;
        }
    }

    /**
     * 单件实列化
     * @return MySqlModel
     */
    public static function M()
    {
        $class = static::class;
        if (empty(self::$instace[$class]))
        {
            self::$instace[$class] = InstanceHolder::getClass($class);
        }
        return self::$instace[$class];
    }

    /**
     * 获取表名
     * @return string
     */
    public function getTbl()
    {
        return $this->tbl;
    }

    /**
     * 获取字段列表
     * @return array
     */
    public function getCols()
    {
        return $this->cols;
    }

    /**
     * 获取主键
     * @return array
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * 连接实例名
     * @return string
     */
    public function getNode()
    {
        return $this->node;
    }

    /**
     * 获取默认对象
     * @return array
     */
    public function getDefault()
    {
        if ($this->default != null)
        {
            return $this->default;
        }
        $this->default = [];
        foreach ($this->cols as $col => $val)
        {
            $this->default[$col] = $val[1];
        }

        return $this->default;
    }

    /**
     * 执行原始SQL语句
     * @param string $sql 待执行的SQL
     * @param bool $read 是否为使用只读连接池
     * @return mixed
     * @throws
     */
    public function doQuery(string $sql, bool $read = false)
    {
        return $this->execute($sql, [], [], $read);
    }

    /**
     * 执行原始的SQL语句（用于列表）
     * @param string $sql 待执行的SQL
     * @param MySqlQuery $query MySQL查询器
     * @param int $size 输出数量，0表示全部
     * @param int $idx 页码位置，从1开始，0表示不使用翻页
     * @return array
     * @throws
     */
    public function doList(string $sql, MySqlQuery $query, $size = 0, int $idx = 0)
    {
        //分析数据条件
        $condition = $query->getCondition();
        $orderby = $query->getOrderBy();

        //合并字符
        $sql = "$sql {$condition['where']}";
        if (!empty($orderby))
        {
            $sql .= " ORDER BY $orderby";
        }
        if ($size > 0)
        {
            if ($idx > 0)
            {
                $offset = $size * ($idx - 1);
                $sql .= " LIMIT $offset,$size";
            }
            else
            {
                $sql .= " LIMIT $size";
            }
        }
        $sql .= ';';

        //执行
        $result = $this->execute($sql, $condition['types'], $condition['values'], true);

        //获取结果，并转化为相应字段类型
        $list = $result['Results'];
        if (count($condition['types']) == 0)
        {
            MySqlUtility::forceToDataType($this->cols, $list);
        }

        //返回
        return $list ?: [];
    }

    /**
     * 最外层的子查询（在getList\getRow\getDistinct之前调用）
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param string $cols 需要输出的字段，逗号分隔
     * @param array $order 排序，['otime' => 1]
     * @param int $size 输出数量，0表示全部
     * @return MySqlModel
     */
    function selectAS(array $where = [], string $cols = '*', array $order = [], int $size = 0) : MySqlModel
    {
        $this->selectAS = [
            'where' => $where,
            'cols' => $cols,
            'order' => $order,
            'size' => $size,
        ];
        return $this;
    }

    /**
     * 获取列表数据
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id=>5, age => 9]]
     * @param string $cols 输出的字段，使用逗号分隔
     * @param array $order 排序 ['odr' => 1, 'atime' => -1]
     * @param int $size 输出数量，0表示全部
     * @param int $idx 页码位置，从1开始，0表示不使用翻页
     * @return array
     * @throws
     */
    function getList(array $where = [], string $cols = '*', array $order = [], $size = 0, int $idx = 0)
    {
        //安全判断
        if (empty($cols))
        {
            $cols = '*';
        }

        //分析数据条件
        $query = new MySqlQuery($this->tbl, $this->cols, $where, $order);
        $condition = $query->getCondition();
        $orderby = $query->getOrderBy();

        //合并字符
        $sql = "SELECT $cols FROM `{$this->tbl}`{$condition['index']} {$condition['where']}";
        if (!empty($orderby))
        {
            $sql .= " ORDER BY $orderby";
        }
        if ($size > 0)
        {
            if ($idx > 0)
            {
                $offset = $size * ($idx - 1);
                $sql .= " LIMIT $offset,$size";
            }
            else
            {
                $sql .= " LIMIT $size";
            }
        }

        //外层包子查询
        if ($this->selectAS != null)
        {
            //输入的参数
            $where = $this->selectAS['where'];
            $order = $this->selectAS['order'];
            $cols = $this->selectAS['cols'];
            $size = $this->selectAS['size'];

            //默认字段
            if (empty($cols))
            {
                $cols = '*';
            }

            //分析数据条件
            $queryAS = new MySqlQuery($this->tbl . '_AS', $this->cols, $where, $order);
            $conditionAS = $queryAS->getCondition();
            $orderbyAS = $queryAS->getOrderBy();

            //合成SQL
            $sql = "SELECT $cols FROM ($sql) AS T";
            $sql .= $conditionAS['where'];

            //排序
            if (!empty($orderbyAS))
            {
                $sql .= " ORDER BY $orderbyAS";
            }

            //数量
            if ($size > 0)
            {
                $sql .= " LIMIT $size";
            }

            //累计types和values
            if (count($conditionAS['types']) > 0)
            {
                $condition['types'] = array_merge($condition['types'], $conditionAS['types']);
            }
            if (count($conditionAS['values']) > 0)
            {
                $condition['values'] = array_merge($condition['values'], $conditionAS['values']);
            }
        }

        //结束符
        $sql .= ';';

        //执行
        $result = $this->execute($sql, $condition['types'], $condition['values'], true);

        //获取结果，并转化为相应字段类型
        $list = $result['Results'];
        if (count($condition['types']) == 0)
        {
            MySqlUtility::forceToDataType($this->cols, $list);
        }

        //返回结果
        return $list ?: [];
    }

    /**
     * 通过条件获取一行数据
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id=>5, age => 9]]
     * @param string $cols 输出的字段，使用逗号分隔
     * @param array $order 排序 ['odr' => 1, 'atime' => -1]
     * @return bool|array
     * @throws
     */
    public function getRow(array $where = [], string $cols = '*', array $order = [])
    {
        $result = $this->getList($where, $cols, $order, 1);
        if (count($result) > 0)
        {
            return $result[0];
        }
        return false;
    }

    /**
     * 通过主键值获取一行数据
     * @param mixed $id 主键值
     * @param string $cols $cols 输出的字段，使用逗号分隔
     * @param array $order 排序 ['odr' => 1, 'atime' => -1]
     * @return bool|mixed
     * @throws
     */
    public function getRowById($id, string $cols = '*', array $order = [])
    {
        $where = $this->getIdCondition($id);
        return $this->getRow($where, $cols, $order);
    }

    /**
     * 通过条件获取一个数据
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id=>5, age => 9]]
     * @param string $col 输出的字段（单个字段），*号则取主键
     * @param array $order 排序 ['odr' => 1, 'atime' => -1]
     * @param mixed $dft 默认值（指定字段记录不存在时返回）
     * @return bool|string
     * @throws
     */
    public function getOne(array $where = [], string $col = '*', array $order = [], $dft = false)
    {
        $col = $col == '*' ? $this->key[0] : $col;
        $result = $this->getRow($where, $col, $order);
        if ($result != false && isset($result[$col]))
        {
            return $result[$col];
        }

        return $dft;
    }

    /**
     * 通过主键值获取一个数据
     * @param mixed $id 主键值
     * @param string $col 输出的字段（单个字段），*号则取主键
     * @param array $order 排序 ['odr' => 1, 'atime' => -1]
     * @param mixed $dft 默认值（指定字段记录不存在时返回）
     * @return bool|string
     * @throws
     */
    public function getOneById($id, string $col = '*', array $order = [], $dft = false)
    {
        $where = $this->getIdCondition($id);
        return $this->getOne($where, $col, $order, $dft);
    }

    /**
     * 合计数量
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param string $cols 输出的字段，，如 qty,amt
     * @return array
     * @throws
     */
    function getSum(array $where, string $cols)
    {
        //安全判断
        if (empty($cols) || $cols == '')
        {
            throw new DbException('cols can\'t be empty!');
        }

        //拆成多个
        $tmp = [];
        $cols = explode(',', $cols);
        foreach ($cols as $col)
        {
            $tmp[] = "SUM($col) as $col";
        }

        //获取数据并返回
        return $this->getList($where, join(',', $tmp));
    }

    /**
     * 获取数量
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param string $cols 输出的字段，默认是*，或 distinct(uid)，只能一个字段
     * @return int
     * @throws
     */
    function getCount(array $where = [], string $cols = '*')
    {
        //分析数据条件
        $query = new MySqlQuery($this->tbl, $this->cols, $where, []);
        $condition = $query->getCondition();

        //合并字符
        $sql = "SELECT COUNT($cols) FROM `{$this->tbl}`{$condition['index']} {$condition['where']};";

        //执行
        $result = $this->execute($sql, $condition['types'], $condition['values'], true);

        //返回结果
        if ($result != false && count($result['Results']) > 0)
        {
            return (int)array_values($result['Results'][0])[0];
        }

        //返回默认值
        return 0;
    }

    /**
     * 获取数据字典对象 key-info 格式（仅能用于少量数据的情况，不推荐在大量数据是使用）
     * @param string $key 字典的KEY
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param string $cols 需要输出的字段，逗号分隔
     * @param array $order 排序 ['odr' => 1, 'atime' => -1]
     * @return array
     * @throws
     */
    function getDict(string $key, array $where = [], $cols = '*', array $order = [])
    {
        //补充where语句和字段
        if (trim($cols) != '*' && strpos($cols, $key) === false)
        {
            $cols .= ',' . $key;
        }

        //获取数据
        $list = $this->getList($where, $cols, $order);
        if ($list == false)
        {
            return [];
        }

        //返回
        return array_column($list, null, $key);
    }

    /**
     * 获取数据字典列表 key-list 格式（仅能用于少量数据的情况，不推荐在大量数据是使用）
     * @param string $key 字典的KEY
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param string $cols 需要输出的字段，逗号分隔
     * @param array $order 排序 ['odr' => 1, 'atime' => -1]
     * @return array
     * @throws
     */
    function getDicts(string $key, array $where = [], $cols = '*', array $order = [])
    {
        //补充where语句和字段
        if (trim($cols) != '*' && strpos($cols, $key) === false)
        {
            $cols .= ',' . $key;
        }

        //获取数据
        $list = $this->getList($where, $cols, $order);
        if ($list == false)
        {
            return [];
        }

        //转成字典
        $dict = [];
        foreach ($list as $value)
        {
            $dict[$value[$key]][] = $value;
        }

        //返回
        return $dict;
    }

    /**
     * 获取某个字段列表（返回一维数组）
     * @param string $col 要输出的字段（只能是一个字段）
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @return array
     */
    function getDistinct(string $col, array $where = [])
    {
        //获取数据
        $list = $this->getList($where, "distinct($col) as col");
        if ($list == false)
        {
            return [];
        }

        //返回
        return array_column($list, 'col');
    }

    /**
     * 通过条件判断是否存在
     * @param array $where 数据条件
     * @return bool
     * @throws
     */
    function exist($where)
    {
        $result = $this->getList($where, '1', [], 1);
        if (count($result) > 0)
        {
            return true;
        }
        return false;
    }

    /**
     * 通过主键值判断是否存在
     * @param mixed $id 主键值
     * @return bool
     * @throws
     */
    function existById($id)
    {
        //数据条件
        $where = $this->getIdCondition($id);

        //返回
        return $this->exist($where);
    }

    /**
     * 插入一条数据
     * @param array $data 需要插入的数据 {xxx:vvv, yy:vv}
     * @param bool $onDuplicateUpdate 重复KEY时是否执行更新
     * @return array|bool
     * @throws
     */
    function insert(array $data, bool $onDuplicateUpdate = false)
    {
        $keyId = $this->key[0];
        $keyType = $this->key[1];

        //组装SQL语句
        $cols = [];
        $marks = [];
        $types = [];
        $values = [];
        foreach ($this->cols as $col => $val)
        {
            //主键字段
            if ($keyId == $col && $keyType == MySqlModel::AutoKeyID)
            {
                continue;
            }

            //获取字段类型
            $types[] = $this->getType($col);

            //普通字段
            $cols[] = $col;
            $marks[] = '?';
            if (isset($data[$col]))
            {
                $values[] = $data[$col];
            }
            else
            {
                $values[] = $val[1];
            }
        }

        //合并字符
        $cols = join('`,`', $cols);
        $marks = join(',', $marks);
        $sql = "INSERT INTO `{$this->tbl}` (`$cols`) VALUES ($marks)";

        //更新部分
        if ($onDuplicateUpdate)
        {
            $cols = [];
            foreach ($data as $col => $val)
            {
                //如果是主键字段
                if ($keyId == $col)
                {
                    continue;
                }

                //其它更新字段
                if (isset($this->cols[$col]))
                {
                    $cols[] = "`$col`=?";
                    $types[] = $this->getType($col);
                    $values[] = $val;
                }
            }
            if (count($cols) > 0)
            {
                $sets = join(',', $cols);
                $sql .= " ON DUPLICATE KEY UPDATE $sets";
            }
        }

        //SQL结束
        $sql .= ';';

        //执行
        $result = $this->execute($sql, $types, $values);

        //调用插入监听
        $this->invokeInsertMonitor($keyId, $data);

        //分析返回结果
        if ($result['AffectedRows'] > 0)
        {
            if ($this->key[1] == MySqlModel::AutoKeyID)
            {
                return $result['InsertId'];
            }
            else
            {
                return $data[$this->key[0]] ?? true;
            }
        }
        return false;
    }

    /**
     * @param array $data 需要插入的数据 {xxx:vvv, yy:vv}
     * @param mixed $onDuplicate 当数据重复后处理逻辑
     * @return array|bool
     * @throws
     */
    function insertOnDuplicate(array $data, $onDuplicate)
    {
        try
        {
            return $this->insert($data);
        }
        catch (DbException $ex)
        {
            if ($ex->getCode() == 1062)
            {
                $onDuplicate($data);
                return $this->insert($data);
            }
            throw $ex;
        }
    }

    /**
     * 批量插入数据
     * @param array $data 批量数据（二维数组）[{xxx:vvv, yy:vv},{}]
     * @param bool $onDuplicateUpdate 重复KEY时是否执行更新
     * @return bool
     * @throws
     */
    function inserts(array $data, bool $onDuplicateUpdate = false)
    {
        $keyId = $this->key[0];
        $keyType = $this->key[1];

        //组装SQL语句
        $cols = [];
        $marks = [];
        $types = [];
        $values = [];
        foreach ($data as $key => $item)
        {
            //组装数值
            $params = [];
            foreach ($this->cols as $col => $val)
            {
                //主键字段
                if ($keyId == $col && $keyType == MySqlModel::AutoKeyID)
                {
                    continue;
                }

                //普通字段
                if ($key == 0)
                {
                    $types[] = $this->getType($col);
                    $cols[] = $col;
                    $marks[] = '?';
                }

                //赋值
                if (isset($item[$col]))
                {
                    $params[] = $item[$col];
                }
                else
                {
                    $params[] = $val[1];
                }
            }

            //更新部分
            if ($onDuplicateUpdate)
            {
                foreach ($item as $col => $val)
                {
                    //如果是主键字段
                    if ($keyId == $col)
                    {
                        continue;
                    }

                    //其它更新内容
                    if (isset($this->cols[$col]))
                    {
                        $params[] = $val;
                    }
                }
            }

            //累加数值
            $values[] = $params;
        }

        //合并字符
        $cols = join('`,`', $cols);
        $marks = join(',', $marks);
        $sql = "INSERT INTO `{$this->tbl}` (`$cols`) VALUES ($marks)";

        //更新部分
        if ($onDuplicateUpdate)
        {
            $cols = [];
            foreach ($data[0] as $col => $val)
            {
                //主键字段
                if ($keyId == $col)
                {
                    continue;
                }

                //更新字段
                if (isset($this->cols[$col]))
                {
                    $cols[] = "`$col`=?";
                    $types[] = $this->getType($col);
                }
            }
            if (count($cols) > 0)
            {
                $sets = join(',', $cols);
                $sql .= " ON DUPLICATE KEY UPDATE $sets";
            }
        }

        //SQL结束
        $sql .= ';';

        //执行
        $result = $this->execute($sql, $types, $values);

        //调用插入监听
        $this->invokeInsertsMonitor($keyId, $data);

        //分析返回结果
        if ($result['AffectedRows'] > 0)
        {
            return $result['Results'];
        }
        return false;
    }

    /**
     * 通过条件更新数据
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param array $data 需要更新的数据 {xxx:vvv, yy:vv}
     * @param array $direct 直接更新的数据 {'hits':'hits+1', 'view':'view-1', 'count':'hits+count'}
     * @return bool
     * @throws
     */
    function update(array $where = [], array $data = [], array $direct = [])
    {
        //分析数据条件
        $query = new MySqlQuery($this->tbl, $this->cols, $where, []);
        $condition = $query->getCondition();

        //组装SQL
        $cols = [];
        $types = [];
        $values = [];
        foreach ($data as $col => $val)
        {
            if (!isset($this->cols[$col]))
            {
                continue;
            }
            $cols[] = "`$col`=?";
            $types[] = $this->getType($col);
            $values[] = $val;
        }

        //叠加更新
        foreach ($direct as $col => $val)
        {
            if (!isset($this->cols[$col]))
            {
                continue;
            }
            $cols[] = "`$col`=$val";
        }

        //合并字符
        $sets = join(',', $cols);
        $sql = "UPDATE `{$this->tbl}`{$condition['index']} SET $sets {$condition['where']};";

        //合并数值
        $types = array_merge($types, $condition['types']);
        $values = array_merge($values, $condition['values']);

        //执行
        $result = $this->execute($sql, $types, $values);

        //调用更新监听
        $this->invokeUpdateMonitor($where, $data, $direct);

        //分析返回结果（有个问题：如果更新的内容不变，是不会返回影响行数）
        if ($result['AffectedRows'] > 0)
        {
            return true;
        }

        return $result['Results'] !== false; // PDO方式如果没有改变数据，会返回空数组；
    }

    /**
     * 通过主键值更新数据
     * @param mixed $id 主键值
     * @param array $data 需要更新的数据 {xxx:vvv, yy:vv}
     * @param array $direct 直接更新的数据 {'hits':'hits+1', 'view':'view-1', 'count':'hits+count'}
     * @return bool
     * @throws
     */
    function updateById($id, array $data, array $direct = [])
    {
        //参数检查
        if (is_array($id))
        {
            throw new DbException('UpdateById ID can\'t be array!');
        }

        //获取数据条件
        $where = $this->getIdCondition($id);

        //执行并返回
        return $this->update($where, $data, $direct);
    }

    /**
     * 通过条件增长数据
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param array $data 需要增长的数据 {xxx:1, yy:-2, zz:'yy+1'}
     * @param array $update 需要更新的数据 {xxx:'xx', yyy:'xx'}
     * @return bool
     * @throws
     */
    function increase(array $where = [], array $data = [], array $update = [])
    {
        //分析数据条件
        $query = new MySqlQuery($this->tbl, $this->cols, $where, []);
        $condition = $query->getCondition();

        //组装SQL
        $updates = [];
        foreach ($data as $col => $val)
        {
            //忽略掉Model中不存在列、值为空或者0的数据
            if (!isset($this->cols[$col]))
            {
                continue;
            }
            if (is_numeric($val))
            {
                $val = $col . '+' . $val;
            }
            $updates[] = "`$col`=$val";
        }
        foreach ($update as $col => $val)
        {
            if (!isset($this->cols[$col]))
            {
                continue;
            }
            $updates[] = "`$col`='$val'";
        }
        $sets = join(',', $updates);
        $sql = "UPDATE `{$this->tbl}`{$condition['index']} SET $sets {$condition['where']};";

        //执行
        $result = $this->execute($sql, $condition['types'], $condition['values']);

        //调用更新监听
        $this->invokeUpdateMonitor($where, $data, $update);

        //分析返回结果（有个问题：如果更新的内容不变，是不会返回影响行数）
        if ($result['AffectedRows'] > 0)
        {
            return true;
        }

        return $result['Results'] !== false; // PDO方式如果没有改变数据，会返回空数组；
    }

    /**
     * 通过主键值增长数据
     * @param mixed $id 主键值
     * @param array $data 需要增长的数据 {xxx:1, yy:-2, zz:yy+1}
     * @param array $update 需要更新的数据 {xxx:'xx', yyy:'xx'}
     * @return bool
     * @throws
     */
    function increaseById($id, array $data = [], array $update = [])
    {
        //参数检查
        if (is_array($id))
        {
            throw new DbException('IncreaseById ID can\'t be array!');
        }

        $where = $this->getIdCondition($id);

        return $this->increase($where, $data, $update);
    }

    /**
     * 通过条件删除数据
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @return bool
     * @throws
     */
    function delete(array $where = [])
    {
        //分析数据条件
        $query = new MySqlQuery($this->tbl, $this->cols, $where, []);
        $condition = $query->getCondition();

        //合并字符
        $sql = "DELETE FROM `{$this->tbl}`{$condition['index']} {$condition['where']};";

        //执行
        $result = $this->execute($sql, $condition['types'], $condition['values']);

        //分析返回结果（有个问题：如果数据不存在，是不会返回影响行数）
        if ($result['AffectedRows'] > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * 通过主键值删除数据
     * @param mixed $id 主键值
     * @return bool
     * @throws
     */
    function deleteById($id)
    {
        //参数检查
        if (is_array($id))
        {
            throw new DbException('DeleteById ID can\'t be array!');
        }

        $where = $this->getIdCondition($id);

        return $this->delete($where);
    }

    /**
     * 联合表查询
     * @param MySqlModel $model 右表对象
     * @param array $on 联合条件，['左表字段' => '右表字段']
     * @return MySqlJoin
     */
    function join(MySqlModel $model, array $on): MySqlJoin
    {
        $on['_'] = 'INNER';
        return new MySqlJoin($this, $model, $on, [], []);
    }

    /**
     * 联合表左查询
     * @param MySqlModel $model 右表对象
     * @param array $on 联合条件，['左表字段' => '右表字段']
     * @return MySqlJoin
     */
    function leftJoin(MySqlModel $model, array $on): MySqlJoin
    {
        $on['_'] = 'LEFT';
        return new MySqlJoin($this, $model, $on, [], []);
    }

    /**
     * 联合表右查询
     * @param MySqlModel $model 右表对象
     * @param array $on 联合条件，['左表字段' => '右表字段']
     * @return MySqlJoin
     */
    function rightJoin(MySqlModel $model, array $on): MySqlJoin
    {
        $on['_'] = 'RIGHT';
        return new MySqlJoin($this, $model, $on, [], []);
    }

    /**
     * 联合表强查询
     * @param MySqlModel $model 右表对象
     * @param array $on 联合条件，['左表字段' => '右表字段']
     * @return MySqlJoin
     */
    function straightJoin(MySqlModel $model, array $on): MySqlJoin
    {
        $on['_'] = 'STRAIGHT';
        return new MySqlJoin($this, $model, $on, [], []);
    }

    /**
     * 联合子表查询
     * @param MySqlModel $model 右表对象
     * @param array $on 联合条件，['左表字段' => '右表字段']
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param string $cols 需要输出的字段，逗号分隔
     * @param array $order 排序，['otime' => 1]
     * @param int $size 输出数量，0表示全部
     * @param int $idx 页码位置，从1开始，0表示不使用翻页
     * @return MySqlJoin
     */
    function joinAS(MySqlModel $model, array $on, array $where = [], string $cols = '*', array $order = [], int $size = 0, int $idx = 0): MySqlJoin
    {
        $on['_'] = 'INNER';
        $as = [
            'where' => $where,
            'cols' => $cols,
            'order' => $order,
            'size' => $size,
            'idx' => $idx
        ];
        return new MySqlJoin($this, $model, $on, $as, []);
    }

    /**
     * 联合子表查询（左联）
     * @param MySqlModel $model 右表对象
     * @param array $on 联合条件，['左表字段' => '右表字段']
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param string $cols 需要输出的字段，逗号分隔
     * @param array $order 排序  ['otime' => 1]
     * @param int $size 输出数量，0表示全部
     * @param int $idx 页码位置，从1开始，0表示不使用翻页
     * @return MySqlJoin
     */
    function leftJoinAS(MySqlModel $model, array $on, array $where = [], string $cols = '*', array $order = [], int $size = 0, int $idx = 0): MySqlJoin
    {
        $on['_'] = 'LEFT';
        $as = [
            'where' => $where,
            'cols' => $cols,
            'order' => $order,
            'size' => $size,
            'idx' => $idx
        ];
        return new MySqlJoin($this, $model, $on, $as, []);
    }

    /**
     * 联合子表查询（右联）
     * @param MySqlModel $model 右表对象
     * @param array $on 联合条件，['左表字段' => '右表字段']
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param string $cols 需要输出的字段，逗号分隔
     * @param array $order 排序  ['otime' => 1]
     * @param int $size 输出数量，0表示全部
     * @param int $idx 页码位置，从1开始，0表示不使用翻页
     * @return MySqlJoin
     */
    function rightJoinAS(MySqlModel $model, array $on, array $where = [], string $cols = '*', array $order = [], int $size = 0, int $idx = 0): MySqlJoin
    {
        $on['_'] = 'RIGHT';
        $as = [
            'where' => $where,
            'cols' => $cols,
            'order' => $order,
            'size' => $size,
            'idx' => $idx
        ];
        return new MySqlJoin($this, $model, $on, $as, []);
    }

    /**
     * 联合表子查询（强联）
     * @param MySqlModel $model 右表对象
     * @param array $on 联合条件，['左表字段' => '右表字段']
     * @param array $where 数据条件 ['id' => 123, 'name' => 'xixi', '$or' => [ id => 5, age => 9]]
     * @param string $cols 需要输出的字段，逗号分隔
     * @param array $order 排序  ['otime' => 1]
     * @param int $size 输出数量，0表示全部
     * @param int $idx 页码位置，从1开始，0表示不使用翻页
     * @return MySqlJoin
     */
    function straightJoinAS(MySqlModel $model, array $on, array $where = [], string $cols = '*', array $order = [], int $size = 0, int $idx = 0): MySqlJoin
    {
        $on['_'] = 'STRAIGHT';
        $as = [
            'where' => $where,
            'cols' => $cols,
            'order' => $order,
            'size' => $size,
            'idx' => $idx
        ];
        return new MySqlJoin($this, $model, $on, $as, []);
    }

    /**
     * 获取主键值的数据条件
     * @param mixed $id 主键值
     * @return array
     */
    private function getIdCondition($id)
    {
        $col = $this->key[0];
        return [
            $col => $id
        ];
    }

    /**
     * 获取字段类型
     * @param string $key 字段名称
     * @return string
     */
    private function getType(string $key)
    {
        return $this->cols[$key][0];
    }

    /**
     * 调用插入监听，并回调结果（单条插入）
     * @param string $keyId 主键字段名
     * @param array $data 插入的数据对象
     */
    private function invokeInsertMonitor(string $keyId, array $data)
    {
        //是否实例化监控
        if (!$this->monitor)
        {
            return;
        }

        //判断是否有插入监听
        $reg = $this->monitor->registerInsertMonitor();
        if ($reg == false)
        {
            return;
        }

        //计算出监听字段的值
        $callbackKey = $data[$keyId] ?? $this->cols[$keyId][1];
        $callbackData = $this->getInvokeCallbackData($reg, $data);

        //使用异步回调
        Tasker::deliver(static::class, 'insertMonitorCallback', [$callbackKey, $callbackData]);
    }

    /**
     * 调用插入监听，并回调结果（批量插入）
     * @param string $keyId 主键字段名
     * @param array $data 插入的数据对象
     */
    private function invokeInsertsMonitor(string $keyId, array $data)
    {
        //是否实例化监控
        if (!$this->monitor)
        {
            return;
        }

        //判断是否有插入监听
        $reg = $this->monitor->registerInsertMonitor();
        if ($reg == false)
        {
            return;
        }

        //计算出监听字段的值
        $callbackKeys = [];
        $callbackDatas = [];
        foreach ($data as $value)
        {
            $callbackKeys[] = $value[$keyId] ?? $this->cols[$keyId][1];;
            $callbackDatas[] = $this->getInvokeCallbackData($reg, $value);
        }

        //使用异步回调
        Tasker::deliver(static::class, 'insertMonitorCallback', [$callbackKeys, $callbackDatas]);
    }

    /**
     * 调用更新监听，并回调结果
     * @param array $where
     * @param array $data
     * @param array $direct
     */
    private function invokeUpdateMonitor(array $where, array $data, array $direct)
    {
        //是否实例化监控
        if (!$this->monitor)
        {
            return;
        }

        //判断是否有更新监听
        $reg = $this->monitor->registerUpdateMonitor();
        if ($reg == false)
        {
            return;
        }

        //计算出监听字段的值
        $callbackData = $this->getInvokeCallbackData($reg, array_merge($data, $direct));

        //使用异步回调
        Tasker::deliver(static::class, 'updateMonitorCallback', [$where, $callbackData]);
    }

    /**
     * 获取监听回调数据
     * @param array|bool $reg 监听对象
     * @param array $data 原始执行的数据
     * @return array
     */
    private function getInvokeCallbackData($reg, array $data)
    {
        if (is_array($reg))
        {
            $tmp = [];
            foreach ($reg as $col)
            {
                if (isset($data[$col]))
                {
                    $tmp[$col] = $data[$col];
                }
            }
            return $tmp;
        }
        return $data;
    }
}
