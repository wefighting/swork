<?php
namespace Swork\Pool\Amqp;

use Swork\Pool\AbstractConfig;

class AmqpConfig extends AbstractConfig
{
    /**
     * 是否启用消费队列
     * @return string
     */
    public function isEnable()
    {
        return $this->confg['enable'] ?? false;
    }

    /**
     * 是否使用多线程执行任务
     * @return bool|mixed
     */
    public function isThreads()
    {
        return $this->confg['threads'] ?? false;
    }

    /**
     * 获取所有的消费者
     * @return array
     */
    public function getConsumers()
    {
        return $this->confg['consumers'] ?? [];
    }
}
