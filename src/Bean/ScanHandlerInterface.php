<?php
namespace Swork\Bean;

interface ScanHandlerInterface
{
    /**
     * 解析一条函数之后
     * @param \ReflectionClass $rc
     * @param \ReflectionMethod $method
     * @param string $route
     * @return mixed
     */
    public function afterAnalyzeMethod(\ReflectionClass $rc, \ReflectionMethod $method, string $route);
}