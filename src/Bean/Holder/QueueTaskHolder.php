<?php
namespace Swork\Bean\Holder;

/**
 * QueueTask收集器
 * Class QueueTaskHolder
 * @package Swork\Bean\Holder
 */
class QueueTaskHolder
{
    /**
     * @var array
     */
    private static $holder = [];

    /**
     * 设置容器值
     * @param array $holder
     */
    public static function setHolder(array $holder)
    {
        foreach ($holder as $cls => $item)
        {
            self::$holder[$cls] = $item;
        }
    }

    /**
     * 获取容器值
     * @return array
     */
    public static function getHolder(): array
    {
        return self::$holder;
    }
}
