<?php
namespace Swork\Process;

/**
 * 进程回调接口
 * Interface ProcessHandlerInterface
 */
interface ProcessHandlerInterface
{
    /**
     * @param \swoole_websocket_server $server
     * @param \swoole_process $process
     * @param array $exts
     */
    public function run(\swoole_websocket_server $server, \swoole_process $process, array $exts = []);
}
