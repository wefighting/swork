<?php
namespace Swork\Helper;

/**
 * 数字格式化工具
 * Class NumberHelper
 * @package Swork\Helper
 */
class NumberHelper
{
    /**
     * 保留小数点后N位
     * @param $val
     * @param int $dec 小数位数（默认2位）
     * @param bool $float 是否转换成float类型（默认false值）
     * @return float
     */
    public static function format($val, int $dec = 2, bool $float = false)
    {
        if (is_string($val))
        {
            $val = floatval($val);
        }

        //如果是0
        if ($val == 0)
        {
            return 0;
        }

        //保留N位小数和格式化
        if ($float == true)
        {
            return round($val, $dec);
        }
        else
        {
            return number_format($val, $dec);
        }
    }

    /**
     * 计算相除
     * @param int|float $val1 被除数
     * @param int|float $val2 除数
     * @param int $dec 保留小数位数
     * @param bool $thousands_sep 是否使用千分位
     * @return float|int
     */
    public static function Division($val1, $val2, int $dec = 2, bool $thousands_sep = false)
    {
        if ($val1 == 0 || $val2 == 0)
        {
            return 0;
        }
        $val = round($val1 / $val2, $dec);
        if ($thousands_sep == true)
        {
            $val = number_format($val, $dec);
        }
        return $val;
    }

    /**
     * 计算百分比
     * @param int|float $val1 分子
     * @param int|float $val2 分母
     * @param int $dec 保留小数位数
     * @param string $def 默认值
     * @return string
     */
    public static function CalculatePercent($val1, $val2, int $dec = 2, string $def = '-')
    {
        if ($val1 == 0 || $val2 == 0)
        {
            return $def;
        }
        return round(100 * $val1 / $val2, $dec) . '%';
    }

    /**
     * 计算千分比
     * @param int|float $val1 分子
     * @param int|float $val2 分母
     * @param int $dec 保留小数位数
     * @param string $def 默认值
     * @return string
     */
    public static function CalculatePerThousand($val1, $val2, int $dec = 2, string $def = '-')
    {
        if ($val1 == 0 || $val2 == 0)
        {
            return $def;
        }
        return round(1000 * $val1 / $val2, $dec) . '‰';
    }

    /**
     * 计算万分比
     * @param int|float $val1 分子
     * @param int|float $val2 分母
     * @param int $dec 保留小数位数
     * @param string $def 默认值
     * @return string
     */
    public static function CalculateTenThousand($val1, $val2, int $dec = 2, string $def = '-')
    {
        if ($val1 == 0 || $val2 == 0)
        {
            return $def;
        }
        return round(10000 * $val1 / $val2, $dec) . '‱';
    }
}
