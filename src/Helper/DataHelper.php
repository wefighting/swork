<?php
namespace Swork\Helper;

/**
 * 数据处理器
 * Class DataHelper
 * @package Swork\Helper
 */
class DataHelper
{
    /**
     * 确保输入类型是按默认值的返回
     * @param mixed $value 获取到的值
     * @param mixed $refer 参考的值
     * @return mixed
     */
    public static function EnsureType($value, $refer)
    {
        if ($refer === null)
        {
            return $value;
        }
        if (is_int($refer))
        {
            return intval($value);
        }
        if (is_float($refer))
        {
            return floatval($value);
        }
        if (is_bool($refer))
        {
            return boolval($value);
        }
        if (is_array($refer))
        {
            $tmp = [];
            $len = count($refer);
            if ($len == 0)
            {
                return is_array($value) ? $value : [];
            }
            if (is_int($refer[0]))
            {
                for ($idx = 0; $idx < $len; $idx++)
                {
                    $tmp[] = intval($value[$idx] ?? 0);
                }
            }
            elseif (is_float($refer[0]))
            {
                for ($idx = 0; $idx < $len; $idx++)
                {
                    $tmp[] = floatval($value[$idx] ?? 0.0);
                }
            }
            elseif (is_bool($refer[0]))
            {
                for ($idx = 0; $idx < $len; $idx++)
                {
                    $tmp[] = boolval($value[$idx] ?? false);
                }
            }
            else
            {
                for ($idx = 0; $idx < $len; $idx++)
                {
                    $tmp[] = $value[$idx] ?? '';
                }
            }
            return $tmp;
        }
        return trim($value);
    }
}
